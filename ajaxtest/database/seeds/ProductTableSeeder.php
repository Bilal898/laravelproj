<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \App\Product::create([
        'productname' => 'vitamins',
        'qty' => '1',
        'price' => '2',
        'product_cat_id' => '1',
    ]);
    \App\Product::create([
        'productname' => 'dumbell',
        'qty' => '1',
        'price' => '2',
        'product_cat_id' => '1',
    ]);
    \App\Product::create([
        'productname' => 'mat',
        'qty' => '1',
        'price' => '2',
        'product_cat_id' => '2',
    ]);
    }
}
