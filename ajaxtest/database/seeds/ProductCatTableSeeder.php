<?php

use Illuminate\Database\Seeder;

class ProductCatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \App\ProductCat::create([
        'product_cat_name' => 'health',
    ]);
    \App\ProductCat::create([
        'product_cat_name' => 'home',
    ]);
    }
}
