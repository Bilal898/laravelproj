<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
  </head>
  <body>
    <span>Product Category: </span>
     <select style="width: 200px" class="productcategory" id="prod_cat_id">

         <option value="0" disabled="true" selected="true">-Select-</option>
         @foreach($prod as $cat)
             <option value="{{$cat->id}}">{{$cat->product_cat_name}}</option>
         @endforeach

     </select>

     <span>Product Name: </span>
     <select style="width: 200px" class="productname">

         <option value="0" disabled="true" selected="true">Product Name</option>
     </select>

     <span>Product Price: </span>
     <input type="text" class="prod_price">

     <script type="text/javascript">
         $(document).ready(function(){
$(document).on('change','.productcategory',function(){
   $.ajax({
      type:'get',
      url:'/findProductName',
      success:function(data){
         console.log('success');
      },
      error:function(){
        console.log('hmm1');
      },
   });
   });
   });

</script>
     </script>
  </body>
</html>
